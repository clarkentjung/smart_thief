package smart.thief.smart_thief;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class SmartThief {

	public void getMaxProfit() {

		// read in json input file to create houseValues
		List<Integer> houseValues = getHouseValues(); // complexity: O(n)
		int[] values = new int[houseValues.size()];

		BufferedWriter writer = null;

		try {
			// initiate a writer to write an output file
			writer = new BufferedWriter(new FileWriter("src/main/resources/output.txt"));

			// convert List to array
			for (int i = 0; i < values.length; i++) { // complexity: O(n)
				values[i] = houseValues.get(i);
			}

			/*
			 * corner case: when the size is 0, do nothing when the size is 1, write YES
			 * when the size is 2, write YES for the bigger one.
			 */
			if (values.length == 1) {
				writer.write("YES");
			} else if (values.length == 2) {
				if (values[0] > values[1]) {
					writer.write("YES" + "\n");
					writer.write("NO");
				} else {
					writer.write("NO" + "\n");
					writer.write("YES");
				}
			} else {

				/*
				 * As these houses are in a circle, get the max profit of [0th to (n-2)th] and
				 * that of [1st to (n-1)th] and take the bigger one
				 */
				int[] firstDP = getMaxProfit(values, 0, values.length - 2); // complexity: O(n)
				int[] secondDP = getMaxProfit(values, 1, values.length - 1); // complexity: O(n)

				// make sure firstDP is always the bigger one
				if (secondDP[secondDP.length - 1] > firstDP[firstDP.length - 2]) {
					firstDP = secondDP;
				}

				/*
				 * i-th element of robbedValues is the amount of the robbed value from the i-th
				 * house.
				 */
				int[] robbedValues = getRobbedValues(values, firstDP); // complexity: O(n)

				boolean isFirstLine = true;
				for (int robbedValue : robbedValues) { // complexity: O(n)
					if (isFirstLine) {
						isFirstLine = false;
					} else {
						writer.write("\n");
					}
					// if the robbed value is 0, then the house was skipped.
					if (robbedValue == 0) {
						writer.write("NO");
					} else {
						writer.write("YES");
					}
				}
			}
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private int[] getRobbedValues(int[] values, int[] dp) {

		int index = dp.length - 1;
		if (dp[index] == 0) {
			values[index] = 0;
			index--;
		}
		while (index >= 0) {
			if (index == 0) {
				break;
			} else {
				if (dp[index] == dp[index - 1]) {
					// this means that the index-th house was not robbed.
					values[index] = 0;
					index = index - 1;
				} else {
					/*
					 * this means that the current house was robbed and the previous one was not
					 * robbed.
					 */
					values[index - 1] = 0;
					index = index - 2;
				}
			}
		}

		return values;
	}

	private int[] getMaxProfit(int[] values, int from, int to) {

		// dp[i] represent the maximum value robbed
		// after reaching the i-th house.
		int[] dp = new int[values.length];

		// initialize dp[0] and dp[1]
		dp[0] = values[from];
		dp[1] = Math.max(values[from], values[from + 1]);

		// fill in the entire dp
		for (int i = from + 2; i <= to; i++) {
			// compare the max profit of when we rob the current house,
			// and that of when we don't.
			dp[i - from] = Math.max(values[i] + dp[i - 2 - from], dp[i - 1 - from]);
		}

		return dp;
	}

	private List<Integer> getHouseValues() {
		JSONParser parser = new JSONParser();
		List<Integer> houseValues = new ArrayList<Integer>();

		JSONArray jsonArray;
		try {
			jsonArray = (JSONArray) parser.parse(new FileReader("src/main/resources/input.json"));

			for (Object o : jsonArray) {
				JSONObject house = (JSONObject) o;
				int curValue = ((Long) house.get("value")).intValue();
				houseValues.add(curValue);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return houseValues;
	}

	public static void main(String[] args) {
		SmartThief smartThief = new SmartThief();
		smartThief.getMaxProfit();
	}
}
